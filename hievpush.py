#!/usr/bin/env python3
"""
Script to zip a daily folder of instrumented beehive or apiary data and push
to the HIEv application. Ths is designed to be run on a daily basis and to
capture the previous day's data.
"""

__author__ = "Gerard Devine, HIE"
__version__ = "1.0"
__license__ = "MIT"

import os
import sys
from datetime import datetime, timedelta
from logzero import logger, logfile
import shutil
import hievpy as hp
from pathlib import Path
import tokenfile


# Set up a rotating logfile with 3 rotations, each with a maximum filesize of 1MB:
logfile("/var/www/beehives/code/logfile.log", maxBytes=1e6, backupCount=3)


def main():
    # make sure all folders are in place
    Path('/var/www/beehives/code/zips').mkdir(parents=True, exist_ok=True)
    Path('/var/www/beehives/code/transferred').mkdir(parents=True, exist_ok=True)

    # Initiate Log file
    logger.info("-------------------------------")
    logger.info(f"New Run - Starting {datetime.now().strftime('%d-%m-%Y')}")

    # Calculate yesterday's date
    yesterday = (datetime.now() - timedelta(1)).strftime('%Y%m%d')

    # Search data folder for subfolder named yesterday's date.
    logger.info(f"Searching for a subfolder called {yesterday}")

    if yesterday in os.listdir('/var/www/beehives/code/data'):
        logger.info(f"Directory {yesterday} found")
    else:
        logger.warning(f"Directory {yesterday} not found")
        sys.exit()

    # Zip the folder
    logger.info(f"Zipping Directory {yesterday} to {yesterday}.zip")
    try:
        shutil.make_archive(
            f'/var/www/beehives/code/zips/Hive_X_2020_{yesterday}', 'zip',
            f'/var/www/beehives/code/data/{yesterday}')
        logger.info(f"Zip Directory {yesterday}.zip created")
    except Exception:
        logger.error(f"Error creating Zip file {yesterday}.zip")

    # get hiev token
    token = tokenfile.token

    # upload all files in zip folder to hiev and move to transferred folder
    for zipfile in Path('/var/www/beehives/code/zips').glob('*.zip'):

        # metadata for each file
        metadata = {
            'experiment_id': 142,
            'type': 'RAW',
            'description': 'Hive weight, temperature, relative humidity, ambient temperature and light data at two sites hosting ~50 stingless bee hives on four separate stations (hubs). Data will allow comparisons between hive types and colonies.',
            'contributors': ['Mark Hall', 'James Cook', 'Markus Riegler', 'Robert Spooner-Hart'],
            'start_time': f'{zipfile.stem[-8:-4]}-{zipfile.stem[-4:-2]}-{zipfile.stem[-2:]} 00:00:00',
            'end_time': f'{zipfile.stem[-8:-4]}-{zipfile.stem[-4:-2]}-{zipfile.stem[-2:]} 23:59:59',
            'tags': ['stingless bees', 'hive monitoring', 'hive health', 'forager activity', 'climate adaptation', 'pollination']
        }

        # upload files
        hp.upload(token, 'https://hiev.uws.edu.au/', zipfile, metadata)

        # move file
        shutil.move(zipfile, f'/var/www/beehives/code/transferred/{zipfile.name}')
        shutil.move(f'/var/www/beehives/code/data/{yesterday}', f'/var/www/beehives/code/transferred/{yesterday}')

if __name__ == "__main__":
    """ This is executed when run from the command line """
    main()
