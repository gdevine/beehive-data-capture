'''
Beehive sensor data scraper
===========================

A python flask-based application serving an URL endpoint for accepting posts 
from instrumented beehives at the Hawkesbury Institute for the Environment. 
Data is scraped from the url request and saved to CSV file. 

Author: Gerard Devine
Date: October 2019
'''

import os
import pathlib
import csv
from flask import Flask, request, jsonify
from datetime import datetime

# Import api key from file
from api_key import api_key

app = Flask(__name__)


def create_new_csv(file_path):
    """
    Create a new CSV file with header line
    """
    with open(file_path, 'w', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(["DateTime",
                         "Beehive ID",
                         "Raw Weight",
                         "Brood Temperature",
                         "IAD",
                         "VDD",
                         "Battery Temperature",
                         "External Temperature",
                         "External Humidity",
                         "RSSI",
                         "Status"])


def read_reading(reading_name):
    """
    Returns the value of a given reading name if present, otherwise returns empty string
    """
    try:
        return request.args[reading_name]
    except KeyError:
        return ''


@app.route('/beehives/', methods=['POST'])
def main():
    print('AnbvhgfhgfhjgfhgfhgfhgfhgfhgfhgffhfhfhgfhgfghA')
    # First check that request is authenticated
    headers = request.headers
    auth = headers.get("X-Api-Key")
    if auth != api_key:
        print('BdfgdsghfsfhdsghfxfdxfB')
        return jsonify({"message": f"ERROR: Unauthorized"}), 401

    print('ChgcghfdhjgfjgfdfhjgdgfdhjgfC')
    # Get the current date and current time
    date_now = datetime.today().strftime('%Y%m%d')
    datetime_now = datetime.today().strftime('%Y%m%d %X')

    print('DhgcghfdhjgfjgfdfhjgdgfdhjgfD')
    # Check if a data folder exists for the current date and create if not
    data_dir = os.path.join(app.root_path, 'data', date_now)
    pathlib.Path(data_dir).mkdir(parents=True, exist_ok=True)

    print('EhgcghfdhjgfjgfdfhjgdgfdhjgfE')
    # Scrape incoming request for different readings
    beehive_id = read_reading('beehive_id')
    raw_weight = read_reading('field1')
    temp_brood = read_reading('field2')
    iad = read_reading('field3')
    vdd = read_reading('field4')
    bat_temp = read_reading('field5')
    temp = read_reading('field6')
    hum = read_reading('field7')
    rssi = read_reading('field8')
    status = read_reading('status')

    print('FhgcghfdhjgfjgfdfhjgdgfdhjgfF')
    readings_list = [datetime_now, beehive_id, raw_weight, temp_brood,
                     iad, vdd, bat_temp, temp, hum, rssi, status]

    # Check if a file matching today's date and the current beehive ID
    # exists, and create it (with CSV header line) if not. This should
    # typically happen at the beginning of each new day.
    file_path = os.path.join(app.root_path, 'data',
                             date_now, f'beehive_{beehive_id}_{date_now}.csv')
    if not os.path.isfile(file_path):
        create_new_csv(file_path)

    # Append the current readings
    with open(file_path, 'a', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(readings_list)

    return jsonify({"message": "SUCCESS: Created"}), 201
