'''
Beehive sensor data scraper
===========================

A python flask-based application serving an URL endpoint for accepting posts
from instrumented beehives at the Hawkesbury Institute for the Environment.
Data is scraped from the url request and saved to CSV file.


Variable mappings (As of April 2020)
------------------------------------
field 1:	Weight (kgs)
field 2:	Brood1 Temp (C)
field 3:	Brood2 Temp (C)
field 4:	Hive Box  Temp (C)
field 5:	Hive Box Humidity (%)
field 6:	External Temp (C)
field 7:	External Humidity (%)
field 8:	Apiary Barometric Pressure (hPa)
field 9:	Apiary Light (Lux)
field 10:	N/A Future Use
field 11:	N/A Future Use
field 12:	N/A Future Use
field 13:	IAD (mA)
field 14:	VDD or VAD (V)
field 15:	Battery Temp (C)
field 16:	RSSI (dBm)
field 17:	Status
field 18:	N/A Future Use
field 19:	N/A Future Use
field 20:       N/A Future Use
field 21:       N/A Future Use


Author: Gerard Devine
Date: October 2019
'''

import os
import pathlib
import csv
from flask import Flask, request, jsonify
from datetime import datetime

# Import api key from file
from api_key import api_key

app = Flask(__name__)


def create_new_csv(file_path):
    """
    Create a new CSV file with header line
    """
    with open(file_path, 'w', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(["Date",
                         "Time",
                         "Beehive ID",
                         "Weight (kgs)",
                         "Brood1 Temperature (C)",
                         "Brood2 Temperature (C)",
                         "Hive Box Temperature (C)",
                         "Hive Box Humidity (%)",
                         "External Temperature (C)",
                         "External Humidity (%)",
                         "Apiary Barometric Pressure (hPa)",
                         "Apiary Light (Lux)",
                         "N/A Future Use",
                         "N/A Future Use",
                         "N/A Future Use",
                         "IAD (mA)",
                         "VDD (V)",
                         "Battery Temp (C)",
                         "RSSI (dBm)",
                         "Status",
                         "N/A Future Use",
                         "N/A Future Use",
                         "N/A Future Use",
                         "N/A Future Use"])


def read_reading(reading_name):
    """
    Returns the value of a given reading name if present, otherwise returns empty string
    """
    try:
        return request.args[reading_name]
    except KeyError:
        return 'Key does not exist'


@app.route('/beehives/', methods=['POST'])
def main():
    # First check that request is authenticated
    headers = request.headers
    auth = headers.get("X-Api-Key")
    if auth != api_key:
        return jsonify({"message": f"ERROR: Unauthorized"}), 401

    # Get the current date and current time
    date_now = datetime.today().strftime('%Y%m%d')
    time_now = datetime.today().strftime('%X')

    # Check if a data folder exists for the current date and create if not
    data_dir = os.path.join(app.root_path, 'data', date_now)
    pathlib.Path(data_dir).mkdir(parents=True, exist_ok=True)

    # Scrape incoming request for different readings
    beehive_id = read_reading('beehive_id')
    weight = read_reading('field1')
    brood1_temp = read_reading('field2')
    brood2_temp = read_reading('field3')
    hivebox_temp = read_reading('field4')
    hivebox_hum = read_reading('field5')
    external_temp = read_reading('field6')
    external_hum = read_reading('field7')
    apiary_barpressure = read_reading('field8')
    apiary_light = read_reading('field9')
    unspecified_1 = read_reading('field10')
    unspecified_2 = read_reading('field11')
    unspecified_3 = read_reading('field12')
    iad = read_reading('field13')
    vdd = read_reading('field14')
    bat_temp = read_reading('field15')
    rssi = read_reading('field16')
    status = read_reading('field17')
    unspecified_4 = read_reading('field18')
    unspecified_5 = read_reading('field19')
    unspecified_6 = read_reading('field20')
    unspecified_7 = read_reading('field21')

    readings_list = [date_now, time_now, beehive_id, weight, brood1_temp, brood2_temp, hivebox_temp, hivebox_hum,
                     external_temp, external_hum, apiary_barpressure, apiary_light, unspecified_1, unspecified_2, unspecified_3,
                     iad, vdd, bat_temp, rssi, status, unspecified_4, unspecified_5, unspecified_6, unspecified_7]

    # Check if a file matching today's date and the current beehive ID
    # exists, and create it (with CSV header line) if not. This should
    # typically happen at the beginning of each new day.
    file_path = os.path.join(app.root_path, 'data',
                             date_now, f'beehive_{beehive_id}_{date_now}.csv')
    if not os.path.isfile(file_path):
        create_new_csv(file_path)

    # Append the current readings
    with open(file_path, 'a', newline='') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(readings_list)

    return jsonify({"message": "SUCCESS: Created"}), 201
