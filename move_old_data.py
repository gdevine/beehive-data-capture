from pathlib import Path
from datetime import datetime, timedelta
import shutil

today = datetime.now().strftime('%Y%m%d')
for subdir in Path('data').iterdir():
    if not today in subdir.name:
        shutil.move(subdir, f'transferred/{subdir.name}')        
