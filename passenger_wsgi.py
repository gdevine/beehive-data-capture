import sys, os
INTERP = "/var/www/beehives/code/.env/bin/python"
#INTERP is present twice so that the new Python interpreter knows the actual executable path
if sys.executable != INTERP: os.execl(INTERP, INTERP, *sys.argv)

from main import app as application
