#!/usr/bin/env python3
"""
Script to zip a daily folder of instrumented beehive or apiary data and push
to the HIEv application. This script will deal with the backlog of data prior
to uploading on a daily basis.
"""

__author__ = "Daniel Metzen, HIE"
__version__ = "1.0"
__license__ = "MIT"

from datetime import datetime
import shutil
import hievpy as hp
from pathlib import Path
import tokenfile


def main(pattern='*'):
    # get hiev token
    token = tokenfile.token

    # get todays date
    today = datetime.now().strftime('%Y%m%d')

    # make sure all folders are in place
    Path('zips').mkdir(parents=True, exist_ok=True)
    Path('transferred').mkdir(parents=True, exist_ok=True)

    # zip up each folder but todays and move to zip folder
    for subdir in Path('data').iterdir():
        if not today in subdir.name:
            shutil.make_archive(f'zips/Hive_X_2020_{subdir.name}', 'zip', subdir)

    # upload all files in zip folder to hiev and move to transferred folder
    for zipfile in Path('zips').glob(f'{pattern}.zip'):

        # metadata for each file
        metadata = {
            'experiment_id': 142,
            'type': 'RAW',
            'description': 'Hive weight, temperature, relative humidity, ambient temperature and light data at two sites hosting ~50 stingless bee hives on four separate stations (hubs). Data will allow comparisons between hive types and colonies.',
            'contributors': ['Mark Hall', 'James Cook', 'Markus Riegler', 'Robert Spooner-Hart'],
            'start_time': f'{zipfile.stem[-8:-4]}-{zipfile.stem[-4:-2]}-{zipfile.stem[-2:]} 00:00:00',
            'end_time': f'{zipfile.stem[-8:-4]}-{zipfile.stem[-4:-2]}-{zipfile.stem[-2:]} 23:59:59',
            'tags': ['stingless bees', 'hive monitoring', 'hive health', 'forager activity', 'climate adaptation', 'pollination']
        }

        # upload files
        hp.upload(token, 'https://hiev.uws.edu.au/', zipfile, metadata)

        # move file
        shutil.move(zipfile, f'transferred/{zipfile.name}')


if __name__ == "__main__":
    main(pattern='*20210113*')
